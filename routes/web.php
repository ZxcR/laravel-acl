<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();

Route::get('/', function () {
    return view('welcome');
});

Route::group(['middleware' => 'auth'], function(){
    Route::get('/billing', 'Billing\BillingController@index')->middleware('permission:view-billing');

    Route::get('/acl/role', 'Acl\RoleController@index')->middleware('permission:view-roles');
    Route::get('/acl/role/create', 'Acl\RoleController@create')->middleware('permission:create-roles');
    Route::post ('/acl/role/store', 'Acl\RoleController@store')->middleware('permission:create-roles');
    Route::get('/acl/role/edit/{id}', 'Acl\RoleController@edit')->middleware('permission:edit-roles');
    Route::post('/acl/role/update/{id}', 'Acl\RoleController@update')->middleware('permission:edit-roles');
    Route::get('/acl/role/view/{id}', 'Acl\RoleController@view')->middleware('permission:view-roles');
    Route::delete('/acl/role/{role}/delete', 'Acl\RoleController@delete')->name('acl.role.delete')->middleware('permission:delete-roles');
    Route::post('/acl/role/add-permission/{id}', 'Acl\RoleController@addPermission')->middleware('permission:add-permissions-to-role');

    Route::get('/acl/permission', 'Acl\PermissionController@index')->middleware('permission:view-permissions');
    Route::get('/acl/permission/create', 'Acl\PermissionController@create')->middleware('permission:create-permissions');
    Route::post ('/acl/permission/store', 'Acl\PermissionController@store')->middleware('permission:create-permissions');
    Route::get('/acl/permission/edit/{id}', 'Acl\PermissionController@edit')->middleware('permission:edit-permissions');
    Route::post('/acl/permission/update/{id}', 'Acl\PermissionController@update')->middleware('permission:edit-permissions');
    Route::get('/acl/permission/view/{id}', 'Acl\PermissionController@view')->middleware('permission:view-permissions');
    Route::delete('/acl/permission/{permission}/delete', 'Acl\PermissionController@delete')->name('acl.permission.delete')->middleware('permission:delete-permissions');

    Route::get('/users', 'User\UserController@index')->middleware('permission:view-users');
    Route::get('/user/create', 'User\UserController@create')->middleware('permission:create-users');
    Route::post('/user/store', 'User\UserController@store')->middleware('permission:create-users');
    Route::get('/user/edit/{id}', 'User\UserController@edit')->middleware('permission:edit-users');
    Route::post('/user/update/{id}', 'User\UserController@update')->middleware('permission:edit-users');
    Route::delete('/user/{user}/delete', 'User\UserController@delete')->middleware('permission:delete-users');
});