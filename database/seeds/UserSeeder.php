<?php

use Illuminate\Database\Seeder;
use App\Role;
use App\User;
use App\Permission;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $hr = Role::where('slug','hr')->first();
        $operator = Role::where('slug', 'operator')->first();
        $superadmin = Role::where('slug', 'superadmin')->first();

        $user1 = new User();
        $user1->name = 'HR User';
        $user1->email = 'hr@test.com';
        $user1->password = bcrypt('123456');
        $user1->save();
        $user1->roles()->attach($hr);

        $user2 = new User();
        $user2->name = 'Operator user';
        $user2->email = 'operator@test.com';
        $user2->password = bcrypt('123456');
        $user2->save();
        $user2->roles()->attach($operator);

        $user3 = new User();
        $user3->name = 'superadmin';
        $user3->email = 'superadmin@test.com';
        $user3->password = bcrypt('123456');
        $user3->save();
        $user3->roles()->attach($superadmin);
    }
}
