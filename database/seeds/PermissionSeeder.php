<?php

use App\Permission;
use Illuminate\Database\Seeder;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {   
        $actions = ['create', 'view', 'edit', 'delete'];
        $modules = ['users', 'roles', 'permissions'];

        foreach ($modules as $module) {
            foreach($actions as $action) {
                $p = new Permission();
                $p->name = "$action $module";
                $p->slug =  "$action-$module";
                $p->save();
            }
        }

        $p1 = new Permission();
        $p1->name = "Add permissions to role";
        $p1->slug = "add-permissions-to-role";
        $p1->save();

        $p2 = new Permission();
        $p2->name = "view billing";
        $p2->slug = "view-billing";
        $p2->save();
    }
}