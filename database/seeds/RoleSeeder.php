<?php

use Illuminate\Database\Seeder;
use App\Role;
use App\Permission;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $operator = new Role();
        $operator->name = 'operator';
        $operator->slug = 'operator';
        $operator->save();

        $hr = new Role();
        $hr->name = 'hr';
        $hr->slug = 'hr';
        $hr->save();
        
        $superadmin = new Role();
        $superadmin->name = 'superadmin';
        $superadmin->slug = 'superadmin';
        $superadmin->save();

        $permissions = \App\Permission::pluck('id');

        $superadmin->refreshPermissions($permissions);

    }
}
