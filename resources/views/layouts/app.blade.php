<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="description" content="">
	<meta name="keywords" content="">
	<title>Биллинг</title>
    @section('head')
        <link rel="stylesheet" href="{{ asset('assets/css/vendor.bundle.css') }}">
        <link rel="stylesheet" href="{{ asset('assets/css/app.bundle.css') }}">
		<link rel="stylesheet" href="{{ asset('assets/css/theme-a.css') }}">
    @show
</head>
<body>
	<div id="app_wrapper">

		<header id="app_topnavbar-wrapper">
			<nav role="navigation" class="navbar topnavbar">
				<div class="nav-wrapper">
					<ul class="nav navbar-nav pull-left left-menu">
						<li class="app_menu-open">
							<a href="javascript:void(0)" data-toggle-state="app_sidebar-left-open" data-key="leftSideBar">
								<i class="zmdi zmdi-menu"></i>
							</a>
						</li>
					</ul>
					<ul class="nav navbar-nav pull-right">
						<li class="dropdown avatar-menu">
							@guest
								<a href="/login">Войти</a>
							@endguest
							@auth
							<a href="javascript:void(0)" data-toggle="dropdown" aria-expanded="false">
								<span class="meta">
									<span class="avatar">
										<img src="{{ asset('assets/img/default_user.png') }}" alt="" class="img-circle max-w-35">
									</span>
									<span class="name">{{ Auth::user()->name }}</span>
									<span class="caret"></span>
								</span>
							</a>
							@endauth
							<ul class="dropdown-menu btn-primary dropdown-menu-right">
								<li>
									<a href="#" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><i class="zmdi zmdi-sign-in"></i> Выйти</a>
									<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
										@csrf
									</form>
								</li>
							</ul>
						</li>
					</ul>
				</div>
				<form role="search" action="#" class="navbar-form" id="navbar_form">
					<div class="form-group">
						<input type="text" placeholder="Search and press enter..." class="form-control" id="navbar_search" autocomplete="off">
						<i data-navsearch-close class="zmdi zmdi-close close-search"></i>
					</div>
					<button type="submit" class="hidden btn btn-default">Submit</button>
				</form>
			</nav>
        </header>

		<aside id="app_sidebar-left">
			<div id="logo_wrapper">
				
			</div>
			<nav id="app_main-menu-wrapper" class="fadeInLeft scrollbar">
				<div class="sidebar-inner sidebar-push">
					<ul class="nav nav-pills nav-stacked">
						<li class="sidebar-header">Меню</li>
						
						@can('view-billing')
						<li class="">
							<a href="/billing"><i class="zmdi zmdi-view-dashboard"></i>Биллинг</a>
						</li>
						@endcan
						
						@can('view-users')
						<li class="">
                            <a href="/users"><i class="zmdi zmdi-view-dashboard"></i>Пользователи</a>
                        </li>
						@endcan

                        @can('view-roles')
						<li class="">
                            <a href="/acl/role"><i class="zmdi zmdi-view-dashboard"></i>Роли</a>
                        </li>
						@endcan

						@can('view-permissions')
                        <li class="">
                            <a href="/acl/permission"><i class="zmdi zmdi-view-dashboard"></i>Права доступа</a>
                        </li>
						@endcan
                    </ul>
                </div>
            </nav>
        </aside>

        <section id="content_outer_wrapper" style="padding-bottom: 20px;">
            <div id="content_wrapper" style="min-height: 100vh; height: 100%" class="card-overlay">
                @yield('content')
			</div>

            <footer id="footer_wrapper" style="width: 100%">
                <div class="footer-content p-10">
                    <p class="copy m-0">&copy; Copyright <time class="year"></time> All Rights Reserved</p>
                </div>
            </footer>
        </section>
	</div>

	@section('scripts')
		<script src="{{ asset('assets/js/vendor.bundle.js') }}"></script>
		<script src="{{ asset('assets/js/app.bundle.js') }}"></script>
		<script src="{{ asset('assets/js/custom/validation-translations.js') }}"></script>
	@show
</body>
</html>
