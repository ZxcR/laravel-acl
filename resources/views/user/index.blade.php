@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-md-12">
        
        <a href="/user/create" class="btn btn-primary mb-3">Добавить Пользователи</a>
        <div class="card">
            <header class="card-heading">
                <h2 class="card-title">Пользователи</h2>
            </header>
            <div class="card-body">
                <table class="table table-hover">
                    @foreach ($users as $user)
                        <tr>
                            <td>{{ $user->name }}</td>
                            <td>{{ $user->email }}</td>
                            @can('edit-users')
                            <td>
                                <a href="/user/edit/{{ $user->id }}" title="Редактировать"><i class="zmdi zmdi-edit"></i> </a> 
                            </td>
                            @endcan
                            @can('delete-users')
                            <td>
                                <form action="{{ route('acl.role.delete', $user->id) }}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" title="delete" style="border: none; background-color:transparent;">
                                        <i class="zmdi zmdi-delete"></i>
                                    </button>
                                </form>
                            </td>
                            @endcan
                        </tr>
                    @endforeach
                </table>
            </div>
        </div>
    </div>
</div>

    
@endsection

@section('scripts')
    @parent
@endsection