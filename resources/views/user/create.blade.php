@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-md-12">
        
        <div class="card">
            <header class="card-heading">
                <h2 class="card-title">Добавить Пользователя</h2>
            </header>
            <div class="card-body">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <form action="/user/store" method="post">
                    @csrf
                    <input type="text" name="name" class="form-control" placeholder="name">
                    <input type="email" name="email" class="form-control" placeholder="email">
                    <input type="password" name="password" class="form-control" placeholder="password">
                    <select class="form-control" name="roles[]" multiple="multiple">
                        @foreach ($roles as $role)
                            <option value="{{ $role->id }}">{{ $role->name }}</option>
                        @endforeach
                    </select>
                    <input type="submit" class="btn btn-primary">
                </form>
            </div>
        </div>
    </div>
</div>

    
@endsection

@section('scripts')
    @parent
@endsection