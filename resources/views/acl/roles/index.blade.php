@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-md-12">
        
        <a href="/acl/role/create" class="btn btn-primary mb-3">Добавить роль</a>
        <div class="card">
            <header class="card-heading">
                <h2 class="card-title">Роли</h2>
            </header>
            <div class="card-body">
                <table class="table table-hover">
                    @foreach ($roles as $role)
                        <tr>
                            <td>{{ $role->name }}</td>
                            <td>{{ $role->slug }}</td>
                            <td>
                                <ul class="card-actions icons">
                                    <li class="dropdown">
                                        <a href="javascript:void(0)" data-toggle="dropdown" aria-expanded="false">
                                            <i class="zmdi zmdi-more-vert"></i> 
                                        </a>
                                        <ul class="dropdown-menu btn-primary dropdown-menu-right">
                                            <li> 
                                                <a href="/acl/role/view/{{ $role->id }}"><i class="zmdi zmdi-account-add"></i> Назначить права</a> 
                                            </li>
                                            @can('edit-roles')
                                            <li>
                                                <a href="/acl/role/edit/{{ $role->id }}"><i class="zmdi zmdi-edit"></i> Редактировать</a> 
                                            </li>
                                            @endcan
                                        </ul>
                                    </li>
                                </ul>
                            </td>
                            @can('delete-roles')
                            <td>
                                <form action="{{ route('acl.role.delete', $role->id) }}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" title="delete" style="border: none; background-color:transparent;">
                                        <i class="zmdi zmdi-delete"></i>
                                    </button>
                                </form>
                            </td>
                            @endcan
                        </tr>
                    @endforeach
                </table>
            </div>
        </div>
    </div>
</div>

    
@endsection

@section('scripts')
    @parent
@endsection