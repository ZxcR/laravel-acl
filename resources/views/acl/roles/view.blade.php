@extends('layouts.app')

@section('content')
<div class="row">
    
    <div class="col-md-4">
        <div class="card">
            <header class="card-heading">
                <h2 class="card-title">Роль</h2>
            </header>
            <div class="card-body">
                <table class="table">
                    <tr>
                        <td>ID</td>
                        <td>{{ $role->id }}</td>
                    </tr>
                    <tr>
                        <td>Name</td>
                        <td>{{ $role->name }}</td>
                    </tr>
                    <tr>
                        <td>Slug</td>
                        <td>{{ $role->slug }}</td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    <div class="col-md-8">
        <div class="card">
            <header class="card-heading">
                <h2 class="card-title">Права</h2>
            </header>
            <div class="card-body">
                <form action="/acl/role/add-permission/{{ $role->id }}" method="post">
                    @csrf
                    @foreach ($permissions as $permission)
                        <label for="">
                            <input type="checkbox" name="permissions[]" value="{{ $permission->id }}"
                            @foreach ($role->permissions as $exist_permission)
                                @if ($exist_permission->id == $permission->id)
                                    checked
                                @endif
                            @endforeach
                            >
                            {{ $permission->slug }}
                        </label>
                        <br />
                    @endforeach
                    <input type="submit" value="Сохранить" class="btn btn-primary">
                </form>
            </div>

        </div>
    </div>
</div>

    
@endsection

@section('scripts')
    @parent
@endsection