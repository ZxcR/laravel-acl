@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-md-12">
        
        <div class="card">
            <header class="card-heading">
                <h2 class="card-title">Добавить право доступа</h2>
            </header>
            <div class="card-body">
                <form action="/acl/permission/store" method="post">
                    @csrf
                    <input type="text" name="name" class="form-control" placeholder="name">
                    <input type="text" name="slug" class="form-control" placeholder="slug">
                    <input type="submit" class="btn btn-primary">
                </form>
            </div>
        </div>
    </div>
</div>

    
@endsection

@section('scripts')
    @parent
@endsection