@extends('layouts.app')

@section('content')
<div class="row">
    
    <div class="col-md-4">
        <div class="card">
            <header class="card-heading">
                <h2 class="card-title">Право доступа</h2>
            </header>
            <div class="card-body">
                <table class="table">
                    <tr>
                        <td>ID</td>
                        <td>{{ $permission->id }}</td>
                    </tr>
                    <tr>
                        <td>Name</td>
                        <td>{{ $permission->name }}</td>
                    </tr>
                    <tr>
                        <td>Slug</td>
                        <td>{{ $permission->slug }}</td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>

    
@endsection

@section('scripts')
    @parent
@endsection