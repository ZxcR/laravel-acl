@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-md-12">
        
        <a href="/acl/permission/create" class="btn btn-primary mb-3">Добавить право доступа</a>
        <div class="card">
            <header class="card-heading">
                <h2 class="card-title">Права доступа</h2>
            </header>
            <div class="card-body">
                <table class="table table-hover">
                    @foreach ($permissions as $permission)
                        <tr>
                            <td>{{ $permission->name }}</td>
                            <td>{{ $permission->slug }}</td>
                            <td>
                                <ul class="card-actions icons">
                                    <li class="dropdown">
                                        <a href="javascript:void(0)" data-toggle="dropdown" aria-expanded="false">
                                            <i class="zmdi zmdi-more-vert"></i> 
                                        </a>
                                        <ul class="dropdown-menu btn-primary dropdown-menu-right">
                                            <li> 
                                                <a href="/acl/permission/view/{{ $permission->id }}"><i class="zmdi zmdi-mail-reply"></i> Просмотреть </a> 
                                            </li>
                                            @can('edit-permissions')
                                            <li>
                                                <a href="/acl/permission/edit/{{ $permission->id }}"><i class="zmdi zmdi-archive"></i> Редактировать</a> 
                                            </li>
                                            @endcan
                                        </ul>
                                    </li>
                                </ul>
                            </td>
                            @can('delete-permissions')
                            <td>
                                <form action="{{ route('acl.permission.delete', $permission->id) }}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" title="delete" style="border: none; background-color:transparent;">
                                        <i class="zmdi zmdi-delete"></i>
                                    </button>
                                </form>
                            </td>
                            @endcan
                        </tr>
                    @endforeach
                </table>
            </div>
        </div>
    </div>
</div>

    
@endsection

@section('scripts')
    @parent
@endsection