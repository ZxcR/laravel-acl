@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-md-12">
        
        <div class="card">
            <header class="card-heading">
                <h2 class="card-title">Изменить право доступа</h2>
            </header>
            <div class="card-body">
                <form action="/acl/permission/update/{{ $permission->id }}" method="post">
                    @csrf
                    <input type="text" name="name" class="form-control" placeholder="name" value="{{ $permission->name }}">
                    <input type="text" name="slug" class="form-control" placeholder="slug" value="{{ $permission->slug }}">
                    <input type="submit" class="btn btn-primary">
                </form>
            </div>
        </div>
    </div>
</div>

    
@endsection

@section('scripts')
    @parent
@endsection