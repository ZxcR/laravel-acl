<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <title>Войти</title>
    <link rel="stylesheet" href="{{ asset('assets/css/vendor.bundle.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/app.bundle.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/theme-a.css') }}">
</head>

<body id="auth_wrapper">
    <div id="login_wrapper">
        <div class="logo" style="display: flex; justify-content: center; align-items: center; background-color: #42a5f5">
            
        </div>
        <div id="login_content">
            <h1 class="login-title">Вход</h1>
            <div class="login-body">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                <form id="form-horizontal" class="form-horizontal" method="POST" action="{{ route('login') }}">
                    @csrf
                    <div class="form-group @error('email') has-error @enderror">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="zmdi zmdi-email"></i></span>
                            <input 
                                name="email"
                                type="email"
                                data-rule-required="true"
                                data-rule-rangelength="[5,30]"
                                data-rule-email="true"
                                class="form-control"
                                aria-required="true"
                                value="{{ old('email') }}"
                                placeholder="Email адрес"
                                autofocus>
                            {{-- @error('email')
                                <span class="help-block">{{ $message }}</span>
                            @enderror --}}
                        </div>
                    </div>
                    <div class="form-group @error('password') has-error @enderror">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="zmdi zmdi-lock"></i></span>
                            <input 
                                name="password"
                                type="password"
                                data-rule-required="true"
                                data-rule-rangelength="[6,30]"
                                class="form-control"
                                aria-required="true"
                                placeholder="Пароль">
                            
                            @error('password')
                                <span class="help-block">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                    <button type="submit" class="btn btn-info btn-block m-t-40">Войти</button>
                </form>
            </div>
        </div>
    </div>
    <script src="{{ asset('assets/js/vendor.bundle.js') }}"></script>
    <script src="{{ asset('assets/js/app.bundle.js') }}"></script>
    <script src="{{ asset('assets/js/custom/validation-translations.js') }}"></script>
</body>
</html>
