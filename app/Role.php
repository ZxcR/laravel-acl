<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Permission;

class Role extends Model
{   
    protected $fillable = [
        'name', 'slug'
    ];

    public function permissions()
    {
        return $this->belongsToMany(Permission::class,'roles_permissions');
    }

    public function refreshPermissions($permission_ids)
    {
        $permissions = Permission::getAllPermissions($permission_ids);
        
        $this->permissions()->detach();
        
        if($permissions === null) {
            return false;
        }

        $this->permissions()->saveMany($permissions);
        
        return true;
    }
}
