<?php

namespace App\Http\Controllers\Acl;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Role;
use App\Permission;

class RoleController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $roles = Role::all();

        return view('acl.roles.index', [
            'roles' => $roles,
        ]);
    }

    /**
     * 
     */
    public function create()
    {
        return view('acl.roles.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'slug' => 'required'
        ]);

        Role::create($request->all());

        return redirect("/acl/role");
    }

    public function view(int $id)
    {   
        $role = $this->getRole($id);
        $permissions = Permission::all();

        return view('acl.roles.view', [
            'role' => $role,
            'permissions' => $permissions,
        ]);
    }

    public function edit(int $id)
    {   
        $role = $this->getRole($id);

        return view('acl.roles.edit', [
            'role' => $role
        ]);
    }

    public function update(Request $request, int $id)
    {
        $role = $this->getRole($id);

        $request->validate([
            'name' => 'required',
            'slug' => 'required'
        ]);

        $role->update($request->all());

        return redirect("/acl/role/view/$id");
        
    }

    public function addPermission(Request $request, int $id)
    {
        $role = $this->getRole($id);

        $request->validate([
            'permissions.*' => 'numeric'
        ]);

        $role->refreshPermissions($request->permissions ?? []);

        return redirect("/acl/role");
    }

    public function delete(Role $role)
    {   
        $role->delete();

        return redirect("/acl/role");
    }

    
    private function getRole(int $id)
    {
        return Role::findOrFail($id);
    }


}
