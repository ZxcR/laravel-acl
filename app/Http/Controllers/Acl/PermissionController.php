<?php

namespace App\Http\Controllers\Acl;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Permission;

class PermissionController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $permissions = Permission::all();

        return view('acl.permissions.index', [
            'permissions' => $permissions
        ]);
    }

    public function create()
    {
        return view('acl.permissions.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'slug' => 'required'
        ]);

        Permission::create($request->all());

        return redirect("/acl/permission");
    }

    public function view(int $id)
    {   
        $permission = $this->getPermission($id);
        
        return view('acl.permissions.view', [
            'permission' => $permission,
        ]);
    }

    public function edit(int $id)
    {   
        $permission = $this->getPermission($id);

        return view('acl.permissions.edit', [
            'permission' => $permission
        ]);
    }

    public function update(Request $request, int $id)
    {
        $permission = $this->getpermission($id);

        $request->validate([
            'name' => 'required',
            'slug' => 'required'
        ]);

        $permission->update($request->all());

        return redirect("/acl/permission/view/$id");
        
    }

    public function delete(Permission $permission)
    {   
        $permission->delete();

        return redirect("/acl/permission");
    }

    private function getPermission(int $id)
    {
        return Permission::findOrFail($id);
    }

}
