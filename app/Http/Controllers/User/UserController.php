<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Role;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {   
        $users = User::all();

        return view('user.index', [
            'users' => $users,
        ]);
    }

    /**
     * 
     */
    public function create()
    {   
        $roles = Role::all();

        return view('user.create', [
            'roles' => $roles
        ]);
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:4',
            'roles' => 'required',
        ]);

        $roles = Role::whereIn('id', $request->roles)->get();

        $user = new User();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->save();
        $user->roles()->sync($roles);

        return redirect("/users");
    }

    public function view(int $id)
    {   
        $user = $this->getuser($id);
        $permissions = Permission::all();

        return view('user.view', [
            'user' => $user,
            'permissions' => $permissions,
        ]);
    }

    public function edit(int $id)
    {   
        
        $user = $this->getUser($id);
        $roles = Role::all();

        return view('user.edit', [
            'user' => $user,
            'roles' => $roles
        ]);
    }

    public function update(Request $request, int $id)
    {
        $user = $this->getUser($id);

        $request->validate([
            'name' => 'required',
            'email' => 'required|email|unique:users,email,'.$user->id,
            'password' => 'required|min:4',
            'roles' => 'required',
        ]);

        $roles = Role::whereIn('id', $request->roles)->get();

        $user->email = $request->email;
        $user->name = $request->name;
        $user->password = bcrypt($request->password);
        $user->update();

        $user->roles()->sync($roles);

        return redirect("/users");
        
    }

    public function addPermission(Request $request, int $id)
    {
        $user = $this->getuser($id);

        $request->validate([
            'permissions.*' => 'numeric'
        ]);

        $user->refreshPermissions($request->permissions ?? []);

        return redirect("/users");
    }

    public function delete(user $user)
    {   
        $user->delete();

        return redirect("/users");
    }

    
    private function getUser(int $id)
    {
        return User::findOrFail($id);
    }


}
