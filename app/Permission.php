<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{   
    protected $fillable = ['name', 'slug'];
    
    public function roles()
    {
        return $this->belongsToMany(Role::class,'roles_permissions');
    }

    /**
     * @param array of ids
     * @return mixed
     */
    public static function getAllPermissions($permission_ids)
    {
        return static::whereIn('id', $permission_ids)->get();
    }
}